options(prompt = "useR2020muc> ")

source("user_functions.R")

run_file <- function(filename){
  parsed <- parse(filename)
  
  runtime <- new.env(parent=.GlobalEnv)
  n_expr <- 0
  for ( expression in parsed ){          
    eval(expression, envir=runtime)
    n_expr <- n_expr + 1              
  }
  print(sprintf("Counted %d expressions",n_expr))
  runtime
}


capture <- function(fun, env, name){
  function(...){
    out <- fun(...)
    env[[name]] <- out
    out
  }
}



run_file2 <- function(filename){
  runtime <- new.env(parent=.GlobalEnv)
  store <- new.env()
  
  runtime$start_counting <- capture(start_counting, store, "count")
  runtime$stop_counting  <- capture(stop_counting, store, "count")

  parsed <- parse(filename)
  n_expr <- 0
  for (expression in parsed){
    eval(expression, envir=runtime)
    if ( isTRUE(store$count) ) n_expr <- n_expr + 1
  }
  print(sprintf("Counted %d expressions",n_expr))
  runtime
}

