### A method for deriving information from running R code

A talk at [useR2020muc](https://user2020muc.r-project.org/),
by Mark van der Loo.

#### Abstract

A method for deriving information from running R code

Mark P.J. van der Loo

It is often useful to tap information from a running R script.  Obvious use
cases include monitoring the consumption of resources (time, memory) and
process logging. Perhaps less obvious cases include tracking changes in R
objects or collecting output of unit tests. 

The obvious way to implement such features is to mingle one's main script
with expressions that export the relevant information, for example to screen
or file.

In this talk I demonstrate an elegant programming approach[1] that allows one
to collect such information from the running R script without editing the code
in the scripot. The method is based on a combination of nonstandard evaluation,
functional programming, and a clever use of environments. The result is a
method for building software that allows one to tap information from running R
scripts. The method separates the code that harvests information from the
running R script completely from the user code, while users are able
parameterize the harvesting process without changing a global state such as a
global variable or an option setting.


The method has been applied when implementing the 'lumberjack' package[2] (for
tracking changes in R objects) as well as for the 'tinytest' unit testing
package[3], both of the same author.



References.

[1] MPJ van der Loo (2020), A method for deriving information from running R
code.  The R Journal (accepted). https://arxiv.org/2002.07472

[2] MPJ van der Loo (2020) Monitoring data in R with the 'lumberjack' package
Journal of Statistical Software (submitted)

[3] Mark van der Loo (2019). tinytest: Lightweight and Feature Complete Unit
Testing Framework. R package version 1.1.0.
https://CRAN.R-project.org/package=tinytest


###  Slides and scripts used in the talk.

Here you will also find prototypes of the `capture` function
and the file runners.

To re-enact my recorded demos, you need to 

1. Start an R session
2. `source("functions.R")`

and you are all set.

Happy coding!





